import LinkedList from "./lists/linkedList";
import Stack from "./lists/stack";
import Building from "./building";

const main = () => {
  const arr = [101, 87, 122, 208, 74, 107, 152, 130];
  const arr2 = [78, 72, 84, 66, 68, 26, 37, 34, 51, 100];
  const ARRAY_SIZE = 30;
  const array = getRandomList(ARRAY_SIZE);
  const myList = new LinkedList<number>(array);

  const consoleBlueColor = "\x1b[36m%s\x1b[0m";
  const consoleYelowColor = "\x1b[33m%s\x1b[0m";

  console.log(consoleBlueColor, "The list of buildings given is:\n" + array);

  console.log(consoleYelowColor, "\n\nThe building that everyone sees is:\n" + nextGraterNumber(myList));
};

const getRandomList = (size: number): number[] => {
  return [...Array(size)].map((x) => Math.ceil(Math.random() * 100));
};

const nextGraterNumber = (list: LinkedList<number>) => {
  const answer = new Array<number>(list.size());
  const stack = new Stack<Building>();

  let index = 0;

  let node = list.head;
  answer[index] = 0;

  while (!!node?.next) {
    stack.push(new Building(node.value, index + 1));
    node = node.next;

    while (stack.top() && stack.top().value.hight <= node.value) {
      stack.pop();
    }

    answer[++index] = stack.top()?.value.index || 0;
  }

  return answer;
};

main();
