interface INode<T> {
  value: T;
  next?: Node<T>;
  prev?: Node<T>;
}

export default class Node<T> implements INode<T> {
  public value: T;
  public next?: Node<T>;
  public prev?: Node<T>;

  constructor(value: T, next?:  Node<T>, prev?:  Node<T>) {
    this.value = value;
    this.next = next;
    this.prev = prev;
  }
}
