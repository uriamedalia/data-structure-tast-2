import List from "./list";
import Node from "./listNode";

interface ILinkedList<T> {
  unshift(data: T): Node<T>;
  deleteNode(node: Node<T>): void;
  size(): number;
  search(comparator: (data: T) => boolean): Node<T> | null;
}

export default class LinkedList<T> extends List<T> implements ILinkedList<T> {

  constructor(array: Array<T> = []){
    super();
    array.length && this.push(...array);
  }

  unshift(data: T): Node<T> {
    const node = new Node<T>(data, this.head);
    if (this.head) {
      this.head.prev = node;
    }
    this.head = node;

    return node;
  }

  push(...data: T[]): Node<T> {
    const firstNodeToPush = new Node<T>(data[0]);

    this.chainLinks(firstNodeToPush, data.slice(1));

    if (!this.head) {
      this.head = firstNodeToPush;
      return this.head;
    }

    let currLastNode = this.getLastNode(this.head);
    firstNodeToPush.prev = currLastNode;
    currLastNode.next = firstNodeToPush;

    return firstNodeToPush;
  }

  deleteNode(node: Node<T>): void {
    if (!node.prev) {
      this.head = node.next;
    } else {
      node.prev.next = node.next;
      node.next && (node.next.prev = node.prev);
    }
  }

  size(): number {
    return this.getSize(this.head);
  }

  private getLastNode(node: Node<T>): Node<T> {
    return !!node.next ? this.getLastNode(node.next) : node;
  }
  
  private chainLinks(node: Node<T>, array: Array<T>): Node<T>[] {
    return array.map(value => {
      const newNode = new Node<T>(value);
      node.next = newNode;
      newNode.prev = node;
      node = newNode;
      return node;
    })
  }

  search(comparator: (data: T) => boolean): Node<T> {
    const checkNext = (node: Node<T>): Node<T> | null => {
      if (comparator(node.value)) {
        return node;
      }
      return node.next ? checkNext(node.next) : null;
    };

    return this.head ? checkNext(this.head) : null;
  }

  private getSize(node: Node<T>): number {
    if (!node) return 0;

    return 1 + this.getSize(node.next);
  }

  private hasNext(node: Node<T>): boolean {
    return !!node.next;
  }
}
