import List from "./list";
import Node from "./listNode";

interface IStack<T> {
  pop(): Node<T>;
  top(): Node<T>;
  push(...data: T[]): Node<T>;
}

export default class Stack<T> extends List<T> implements IStack<T> {

  constructor(array: Array<T> = []) { 
    super();
    array.length && this.push(...array)
  }

  push(...data: T[]): Node<T> {
    data.forEach(d => this.pushOne(d));
    
    return this.head;
  }

  pop(): Node<T>{
    const nodeToRemove = this.head;
    this.head = this.head?.prev;
    return nodeToRemove;
  }

  top(): Node<T> {
    return this.head;
  }

  private pushOne(data: T): Node<T> {
    const nodeToPush = new Node<T>(data);
    nodeToPush.prev = this.head;
    this.head = nodeToPush;
    return nodeToPush;
  }
}
