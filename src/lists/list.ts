import Node from "./listNode";

interface IList<T> {
  head: Node<T>;
}

export default abstract class List<T> implements IList<T> {
  public head: Node<T>;

  abstract push(...node: T[]): Node<T>;
}
